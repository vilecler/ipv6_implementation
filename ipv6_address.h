#ifndef IPV6_ADDRESS_H_INCLUDED
#define IPV6_ADDRESS_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

/* Structure ipv6_address
 *
 *  Implements a structure for an IPv6 Address which is equal to 128 bits or 16 bytes.
 */
typedef struct ipv6_address ipv6_address;
struct ipv6_address {
    unsigned short int address[8];   //address length is 128 bits
};

//Convert IPv6 value to string to be readable by humans.
char* ipv6_address_to_str(ipv6_address* address);

//Convert IPv6 value to binary str readably by humans.
char* ipv6_address_to_binary_str(ipv6_address* address);

//Convert string to IPv6 Address to be readable by computers.
ipv6_address* str_to_ipv6_address(char* str);



#endif // IPV6_ADDRESS_H_INCLUDED
