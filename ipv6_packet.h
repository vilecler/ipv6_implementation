#ifndef IPV6_PACKET_H_INCLUDED
#define IPV6_PACKET_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include "ipv6_header.h"
#include "ipv6_address.h"

/* Structure ipv6_packet
 *
 *  Implements a structure for an IPv6 Packet. Minimum MTU is 1 280 bytes
 */
typedef struct ipv6_packet ipv6_packet;
struct ipv6_packet{
    ipv6_header header;     //Header of IPv6 Packet
    void* data;             //Data of IPv6 Packet (size is not constant) Minimum 1240 Bytes
};

//Create a new ipv6 packet from an header and a data
ipv6_packet* create_ipv6_packet(ipv6_header header, void* data);

//Print content of IPv6 Packet.
void debug_ipv6_packet(ipv6_packet* packet);

#endif // IPV6_PACKET_H_INCLUDED
