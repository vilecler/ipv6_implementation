#include <stdio.h>
#include <stdlib.h>
#include "ipv6_address.h"

int main()
{
    printf("Welcome to IPv6 Implementation example.\n");

    char addr_str[] = "2001:0db8:0000:85a3:0000:0000:ac1f:8001";
    printf("String value of str: %s\n", addr_str);

    ipv6_address* addr = str_to_ipv6_address(addr_str); //Converstion

    char *str = ipv6_address_to_str(addr);
    printf("Apres integration: %s \n", str);    //Translation for humans

    char *str_binary = ipv6_address_to_binary_str(addr);
    printf("Apres integration en binaire: %s \n", str_binary);    //Translation for humans in binary

    return 0;
}
