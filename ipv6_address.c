#include "ipv6_address.h"

char* ipv6_address_to_str(ipv6_address* address){
    char *str = malloc(sizeof(char) * (32 + 7 + 1) ); // 32 digits and 7 separators and null char

    for(unsigned int i = 0; i < 8; ++i){
        unsigned short int address_value = address->address[i];
        char word[5];
        sprintf(word, "%x", address_value); //on recupere la valeur du mot hexa
        if(address_value == 0){ //Si 0 le mot vaut 0000
            strcpy(word, "0000");
        }

        //Decalage des digits vers la fin
        if(word[0] != 0 && word[1] != 0 && word[2] != 0 && word[3] == 0){
            word[3] = word[2];
            word[2] = word[1];
            word[1] = word[0];
            word[0] = 48;
        }
        else if(word[0] != 0 && word[1] != 0 && word[2] == 0 && word[3] == 0){
            word[3] = word[1];
            word[2] = word[0];
            word[1] = 48;
            word[0] = 48;
        }
        else if(word[0] != 0 && word[1] == 0 && word[2] == 0 && word[3] == 0){
            word[3] = word[0];
            word[2] = 48;
            word[1] = 48;
            word[0] = 48;
        }
        word[4] = 0; //fin du mot

        //On copie le mot
        str[i*5 + 0] = word[0];
        str[i*5 + 1] = word[1];
        str[i*5 + 2] = word[2];
        str[i*5 + 3] = word[3];
        str[i*5 + 4] = ':';
    }
    str[39] = 0;

    return str;
}

char* ipv6_address_to_binary_str(ipv6_address* address){
    char *str = malloc(sizeof(char) * (128 + 7 + 1)); //128 bits to show and 7 separators

    for(unsigned int i = 0; i < 8; ++i){
        unsigned short int address_value = address->address[i];
        char word[17];

        for(int j = 16; j > 0; --j){
            int state = (address_value >> (j-1)) & 1;

            word[16-j] = (state == 1) ? '1' : '0';
        }
        word[17] = 0;

        str[i*17 + 0] = word[0];
        str[i*17 + 1] = word[1];
        str[i*17 + 2] = word[2];
        str[i*17 + 3] = word[3];
        str[i*17 + 4] = word[4];
        str[i*17 + 5] = word[5];
        str[i*17 + 6] = word[6];
        str[i*17 + 7] = word[7];
        str[i*17 + 8] = word[8];
        str[i*17 + 9] = word[9];
        str[i*17 + 10] = word[10];
        str[i*17 + 11] = word[11];
        str[i*17 + 12] = word[12];
        str[i*17 + 13] = word[13];
        str[i*17 + 14] = word[14];
        str[i*17 + 15] = word[15];
        str[i*17 + 16] = ':';
    }
    str[135] = 0;

    return str;
}

ipv6_address* str_to_ipv6_address(char* str){
    ipv6_address *address = malloc(sizeof(ipv6_address));

    char **values = str_split(str, ':');  //Defined in utils

    for(unsigned int i = 0; i < 8; ++i){
        unsigned int address_value;
        sscanf(values[i], "%x", &address_value);

        address->address[i] = (unsigned short int) address_value;
    }

	return address;
}
