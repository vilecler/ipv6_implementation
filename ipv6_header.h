#ifndef IPV6_HEADER_H_INCLUDED
#define IPV6_HEADER_H_INCLUDED

#include "ipv6_address.h"

/* Structure ipv6_header
 *
 *  Implements a structure for an IPv6 Header which is equal to 320 bits or 40 bytes.
 */
typedef struct ipv6_header ipv6_header;
struct ipv6_header {                        //320 bits = 40 bytes header
    unsigned version:4;                     //4 bits
    char traffic_class;                     //8 bits
    unsigned flow_label:20;                 //20 bits
    unsigned short int playload_length;     //16 bits
    char next_header;                       //8 bits
    char hop_limit;                         //8 bits
    ipv6_address source;                    //128 bits
    ipv6_address destination;               //128 bits
};

//Constant for the version field in IPv6 Header
const unsigned ipv6_version_value = 0x6;

//Values available for the next_header field in IPv6 Header
enum ipv6_next_header_values{
  HopByHopOptionsHeader = 0,
  TCP = 6,
  UDP = 17,
  EncapsulatedIPv6Header = 41,
  RoutingHeader = 43,
  FragmentHeader = 44,
  EncapsulatingSecurityPayloadHeader = 50,
  AuthenticationHeader = 51,
  ICMPv6 = 58,
  NoNextHeader = 59,
  DestinationOptionHeader = 60
};

//Print content of IPv6 Headers
void debug_ipv6_header(ipv6_header* header);

#endif // IPV6_HEADER_H_INCLUDED
