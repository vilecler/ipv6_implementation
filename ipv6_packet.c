#include "ipv6_packet.h"

ipv6_packet* create_ipv6_packet(ipv6_header header, void* data){
    ipv6_packet* packet = malloc(sizeof(header) + sizeof(data));    //A voir si sizeof(data) = 0;
    packet->header = header;
    packet->data = data;

    //TODO check that packet->data exists
    //TODO check size of packet is more or equal to 1 280 bytes or fill with blank.

    return packet;
}

void debug_ipv6_packet(ipv6_packet* packet){
    printf("Debugging IPv6 Packet.\n");
    printf("Size of header: %d\n", sizeof(packet->header));
    printf("Size of data: %d\n", sizeof(packet->data));
    debug_ipv6_header(&packet->header);
    printf("Data: %s\n", packet->data);
}

void debug_ipv6_header(ipv6_header* header){
    printf("Debugging IPv6 Packet Header.\n");
    printf("Version: %d", header->version);
    printf("Traffic Class: %d", header->traffic_class);
    printf("Flow Label: %d", header->flow_label);
    printf("Payload length: %d", header->playload_length);
    printf("Next Header: %d", header->next_header);
    printf("Hop Limit: %d", header->hop_limit);
    printf("Source Address: ");
    //print_ipv6_address(&header->source);
    printf("\nDestination Address: ");
    //print_ipv6_address(&header->destination);
    printf("\n");
}
