#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/* Fonction str_split
 *
 * Permet de d�couper une cha�ne de caract�res selon un d�l�miteur.
 */
char** str_split(char* a_str, const char a_delim);

#endif // UTILS_H_INCLUDED
